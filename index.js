var dsss = [];
const BASE_URL = "https://62f8b76ae0564480352bf5ac.mockapi.io";

// bật loading khi bắt đầu làm việc
let batLoading = () => {
  document.getElementById("loading").style.display = "flex";
};

// tắt loading khi làm việc xong
let tatLoading = () => {
  document.getElementById("loading").style.display = "none";
};

// render danh list sách sinh viên
let renderTalbe = (list) => {
  var contentHTML = "";

  list.forEach((item) => {
    var content = `
    <tr>
        <td>${item.ma}</td>
        <td>${item.ten}</td>
        <td>${item.email}</td>
        <td><img src="${item.hinhAnh}" style="width: 50px" alt="" /></td>
        <td>
        <button onclick="xoaSV('${item.ma}')" class="btn btn-danger">Xóa</button>
        <button onclick="suaSV('${item.ma}')" class="btn btn-warning">Sửa</button>
        </td>
    </tr>
    `;
    contentHTML += content;
  });

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

// render danh sách sinh viên lấy từ mockAPI
let renderDanhSachSinhVien = () => {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      dssv = res.data;
      renderTalbe(dssv);
      console.log("res: ", res);
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};
renderDanhSachSinhVien();

// thêm sinh viên
let themSV = () => {
  batLoading();
  var dataForm = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: dataForm,
  })
    .then((res) => {
      tatLoading();
      renderDanhSachSinhVien();
      console.log("res: ", res);
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

// xóa sinh viên
let xoaSV = (id) => {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      tatLoading();
      renderDanhSachSinhVien();
      console.log("res: ", res);
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

// sửa sinh viên
let suaSV = (id) => {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      showThongTinLenForm(res.data);
      console.log("res: ", res);
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

// cập nhật sinh viên
let capNhatSV = () => {
  batLoading();
  var dataUpdate = layThongTinTuForm();
  var index = dataUpdate.ma;
  axios({
    url: `${BASE_URL}/sv/${index}`,
    method: "PUT",
    data: dataUpdate,
  })
    .then((res) => {
      tatLoading();
      renderDanhSachSinhVien();
      console.log("res: ", res);
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};

let resetForm = () => {
  document.getElementById("formQLSV").reset();
};
