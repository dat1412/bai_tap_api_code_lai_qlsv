let layThongTinTuForm = () => {
  let maSv = document.getElementById("txtMaSV").value;
  let tenSV = document.getElementById("txtTenSV").value;
  let email = document.getElementById("txtEmail").value;
  let hinhAnh = document.getElementById("txtImg").value;

  return {
    ma: maSv,
    ten: tenSV,
    email: email,
    hinhAnh: hinhAnh,
  };
};

let showThongTinLenForm = (data) => {
  document.getElementById("txtMaSV").value = data.ma;
  document.getElementById("txtTenSV").value = data.ten;
  document.getElementById("txtEmail").value = data.email;
  document.getElementById("txtImg").value = data.hinhAnh;
};
